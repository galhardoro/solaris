import { NgModule } from '@angular/core';
import { BackgroundImageDirective } from './background-image/background-image';
import { Directive } from '@angular/core/src/metadata/directives';

@NgModule({
	declarations: [
    BackgroundImageDirective],
	imports: [],
	exports: [
    BackgroundImageDirective]
})
export class DirectivesModule {}
