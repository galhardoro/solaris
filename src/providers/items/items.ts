import { Injectable } from '@angular/core';

import { Item } from '../../models/item';
import { Api } from '../api/api';

@Injectable()
export class Items {
  currentItems: any;
  constructor(public api: Api) { }

  query(query?: any) {
    return this.api.get('api/products').then(res => {
      console.assert(res["data"].length > 0, "Produtos carregados");
    });
  }

  filterProductsByCategoryId(categoryid?: number) {
    return this.loadProducts(categoryid);
  }

  filterProducts(disponibility_id: number): any[] {
    var item_products = [];
    this.currentItems.filter(function (item) {
      item.disponibility.find((item_diponibility) => {
        if (parseInt(item_diponibility.disponibility_id) === disponibility_id) {
          item_products.push(item);
        }
      });
    });
    return item_products;
  }

  loadProducts(categoryid: number): any {
    this.api.get('api/products').then(res => {
      this.currentItems = res["data"];
      this.currentItems = this.filterProducts(categoryid);
      console.log(this.currentItems);
    });
  }


  add(item: Item) {
  }

  delete(item: Item) {
  }

}
