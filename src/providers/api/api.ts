import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  //url: string = 'http://d62bc750.ngrok.io'//'http://localhost:61655';
  url: string = 'https://svc.solarisbrasil.com.br/solaris-microservice';
  svc_path = '/api/mobile';
  headers = new Headers();
  constructor(public http: HttpClient) {
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params.set(k, params[k]);
      }
    }

    reqOpts.headers = new Headers();
    reqOpts.headers.set('Access-Control-Allow-Origin', '*');

    return new Promise(resolve => {
      this.http.get(this.url + '/' + endpoint, reqOpts)
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
          console.warn('API indisponível no momento');
        });
    });

  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return new Promise(resolve => {
      this.http.post(this.url + '/' + endpoint, body, reqOpts)
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
          console.warn('API indisponível no momento');
        });
    });
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return new Promise(resolve => {
      this.http.put(this.url + '/' + endpoint, body, reqOpts)
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
          console.warn('API indisponível no momento');
        });
    });
  }

  delete(endpoint: string, reqOpts?: any) {
    return new Promise(resolve => {
      this.http.delete(this.url + '/' + endpoint, reqOpts)
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
          console.warn('API indisponível no momento');
        });
    });
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return new Promise(resolve => {
      this.http.put(this.url + '/' + endpoint, body, reqOpts)
        .subscribe(data => {
          resolve(data);
        }, err => {
          console.log(err);
          console.warn('API indisponível no momento');
        });
    });
  }
}
