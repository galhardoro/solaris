import { HTTP } from '@ionic-native/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';

/**
 * Api Solaris Internal REST Api handler
 */
@Injectable()
export class SolarisApi {
    // url: string = 'https://hom-svc.solarisbrasil.com.br';
    // svc_path: string = '/ServiceMiddleWare/ServiceMiddleWare.svc'
    // https://hom-svc.solarisbrasil.com.br/solaris-microservice
    // PRD: https://svc.solarisbrasil.com.br/solaris-microservice
    // HML: https://hom-svc.solarisbrasil.com.br/solaris-microservice
    url: string = 'https://svc.solarisbrasil.com.br/solaris-microservice';
    svc_path = '/api/called';
    headers = new Headers();
    constructor(
        public httpCli: HttpClient,
        public http:  HTTP
      ) {
        this.headers.append('Access-Control-Allow-Origin', '*');
        this.headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    }

    /**
     * Obtem o token para autenticação no srv da solaris.
     * Ex de request raw/json body
     * {
     *  "userapp":"APPSOLARIS_01",
     *  "password":"123"
     * }
     * @param body
     * @param reqOpts
     */

    getToken(body: any, reqOpts?: any) {
        return new Promise(resolve => {
            this.httpCli.post(this.url + this.svc_path + '/get-token', body, reqOpts)
                .subscribe(data => {
                    // alert('PROVIDER GET TOKEN: '+data+ '\ntokenUser: '+data['Value.tokenUser']);
                    console.log('PROVIDER GET TOKEN: '+data['value'], 'data: ', data)
                    resolve(data);
                }, err => {
                    console.error("ERRO getToken : ", err);
                    console.log('erro ao consultar API getToken'+ err.message);
                });
        });
    }

    /**
     * Obtem os dados do cliente relacionado ao nº da maquina
     * Ex de request raw/json body
     * {
     *  "InternalNumber":"E0261",
     *  "token": "YUs0LzJObEJnYXRLcVU..."
     * }
     * @param body
     * @param reqOpts
     */


    findFleetByIntenalNumber(body: any, reqOpts?: any) {
        return new Promise(resolve => {
            this.httpCli.post(this.url + this.svc_path + '/fleet/by-internalnumber', body, reqOpts)
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    console.error("ERRO findFleetByIntenalNumber : ", err);
                    console.log('erro ao consultar API findFleetByIntenalNumber: ' +err.message);
                });
        });
    }


    /**
     * Obtem os tipos de problemas relacionaso pela familia do produto.
     * Ex de request raw/json body
     * {
     *  "LineProduct":"{{ID_LINHAPRODUTO}}",
     *  "token": "YUs0LzJObEJnYXRLcVU..."
     * }
     * @param body
     * @param reqOpts
     */
    findProblemFleet(body: any, reqOpts?: any) {
        return new Promise(resolve => {
            this.httpCli.post(this.url + this.svc_path + '/fleet/find-problem', body, reqOpts)
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    console.error("ERRO findProblemFleet : ", err);
                    console.log('erro ao consultar API findProblemFleet: ' +err.message);
                });
        });
    }


    /**
     * Efetua um Post de um chamado.
     * Ex de request raw/json body
     * {
     *  "token": "YUs0LzJObEJnYXRLcVU..."
     *  "Name":"teste",
     *  "Phone":"1111111",
     *  "Email":"teste",
     *  "DescriptionProblem":"Vazamento",
     *  "InternalNumber": "E0261"
     * }
     * @param body
     * @param reqOpts
     */
    openCases(body: any, reqOpts?: any) {
        return new Promise(resolve => {
            this.httpCli.post(this.url + this.svc_path + '/fleet/open-case', body, reqOpts)
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    console.error("ERRO openCase : ", err);
                    console.log('erro ao consultar API openCases: '+err.message);
                });
        });
    }

      /**
     * Efetua um Post de ajuda.
     * Ex de request raw/json body
     * {
     *  "nome": "Rodrigo Galhardo",
     *  "email": "galhardo.dn@gmail.com",
     *  "empresa": "Trnds",
     *  "cnpj": "",
     *  "telefone": "11970849131",
     *  "mensagem": "Teste de envio de email para aplicativo novo solaris."
     * }
     * @param body
     * @param reqOpts
     */
    contactUs(body: any, reqOpts?: any) {
        return new Promise(resolve => {
            this.httpCli.post(this.url + '/api/mail/contact-us', body, reqOpts)
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    console.error("ERRO contact-us : ", err);
                    console.log('erro ao consultar API contact-us: '+err.message);
                });
        });
    }

}
