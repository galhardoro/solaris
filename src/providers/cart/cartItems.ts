import { Injectable } from '@angular/core';

import { CartItem } from '../../models/cartItem';
import { Api } from '../api/api';

@Injectable()
export class CartItems {
  cartItems: any;
  constructor(public api: Api) { }

  query(query?: any) {
    return this.api.get('api/products').then(res => {
      console.assert(res["data"].length > 0, "Produtos carregados");
    });
  }
  
  loadProducts(categoryid: number): any {
    this.api.get('api/products').then(res => {
      this.cartItems = res["data"];
    //   this.cartItems = this.filterProducts(categoryid);
      console.log(this.cartItems);
    });
  }


  add(item: CartItem) {
  }

  delete(item: CartItem) {
  }

}
