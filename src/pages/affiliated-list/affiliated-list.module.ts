import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AffiliatedListPage } from './affiliated-list';

@NgModule({
  declarations: [
    AffiliatedListPage,
  ],
  imports: [
    IonicPageModule.forChild(AffiliatedListPage),
    TranslateModule.forChild()
  ],
  exports: [
    AffiliatedListPage
  ]
})
export class AffiliatedListPageModule {}
