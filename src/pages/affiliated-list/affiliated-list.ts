

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';


import { Api } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-affiliated-list',
  templateUrl: 'affiliated-list.html',
})
export class AffiliatedListPage {
  message: any;
  name: any;
  // statesUF: StatesUf[];
  // cities: Cities[];
  citiesSelected:any[];
  affiliateds: any;

  state: any;
  states:any;

  city: any;
  cities: any;

  tabTitleList: any =  "LISTA";
  tabTitleMap: any =  "MAPA";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public api: Api) {

      this.listAffiliates();
      this.presentLoadingCustom();
      this.tabTitleList = "LISTA";
      this.tabTitleMap = "MAPA"

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AffiliatedPage');
    this.presentLoadingCustom();
  }

  listAffiliates(): void {
    var affiliatedsList = [];
    this.api.get('api/mobile/affiliated').then(res => {

      console.log('filiais: ', res["data"]);
      if(res && res["data"]){
        res["data"].forEach(elem => {
          var affiliated = {
            id: elem.id,
            active: elem.active,
            name: elem.name,
            description: elem.description,
            address: elem.address,
            phone: elem.phone,
            uf: elem.uf,
            lat: elem.lat,
            lon: elem.lon,
            image: elem.image
          }

          affiliatedsList.push(affiliated);

        });

        this.affiliateds =  affiliatedsList;
        var _states = [];
        var _cities = [];

        affiliatedsList.forEach((value, index) => {
          let name_state_city: any[] = value.name.replace(" ", "").replace(" ", "").split("/");

          // console.log('index: ', index, 'value: ', value);

          var state = {
            uf: value.uf,
            name: name_state_city[1]
          }
          _states.push(state);

          // console.log('states: ', _states);

          var city = {
            uf: value.uf,
            name: name_state_city[0]
          }
         _cities.push(city);
        //  console.log('cities: ', _cities)

        });
        this.states = _states;
        this.cities = _cities;

      } else {
        console.error('retorno inválido: ', res, res["data"]);
      }


    }).catch(error =>{
      console.error('error ao obter retorno da API de filiais ', error);
    });
  }

  listCitiesFromUf(codUf:string){
    this.citiesSelected = this.cities.filter((citi)=>{
      return (citi.stateUFId == codUf)
    });
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span
        class="spinner
        spinner-large
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });
  }

  showAffiliatedDetail(affiliated) {
     if(affiliated){
      this.name = affiliated.name;

      this.message =
      // 'Nome <p>'+this.name+'</p>'+
      'Descrição '+affiliated.description+''+
      'Endereço <p>'+affiliated.address+'</p>'+
      'Telefone <p>'+affiliated.phone+'</p>'
     } else {
       console.error('naõ foi encontrada a filial');
     }

    let alert = this.alertCtrl.create({
      title: ''+this.name,
      subTitle: ''+this.message,
      buttons: ['FECHAR'],
      cssClass: 'alert-affiliated'
    });
    alert.present();
  }


}
