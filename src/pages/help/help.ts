import { EmailComposer } from '@ionic-native/email-composer';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { HelpFormService } from './../../services/helpform';
import { ToastController } from 'ionic-angular';
import { SolarisApi } from "../../providers/api/solarisapi";

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {
  private data: any;
  private isSubmitted: Boolean = false;
  private emailAvaliable: Boolean = false;
  private message: String = '';
  private showValidation: Boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formData: HelpFormService,
    private emailComposer: EmailComposer,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public solarisApi: SolarisApi
  ) {
    this.formData = formData;
    this.data = {
      name: '',
      email: '',
      phone: '',
      message: ''

    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');

  }

  onSubmit(myForm) {
    this.isSubmitted = true;
    console.log('onSubmit');
    console.log(myForm);

    if ((myForm.valid)) {
      this.formData.name = this.data.name;
      this.formData.email = this.data.email;
      this.formData.phone = this.data.phone;
      this.formData.message = this.data.message
      this.message = 'Formulário válido.'; // toast
      this.presentToast();

      this.sendEmail();

    } else {
      console.error('formulário inválido, ', myForm);
      this.message = 'Por favor verifique todos os campos.';
      this.presentToast();

    }


  }
  noSubmit(e) {
    e.preventDefault();
  }

  checkEmailAvaliable() {
    this.emailComposer.isAvailable()
    .then((avaliable: boolean) => {
      if(avaliable){
        console.log('Email  está disponivel');
        this.sendEmail();
      }
    }).catch((error) => {
      alert('Email não está disponivel, '+ error);
      this.emailAvaliable = false;
    });
  }

  sendEmail(){
    this.loading(true);

    this.solarisApi
      .contactUs(this.data)
      .then(res => {
        this.message = "Mensagem enviado com sucesso !";
        console.log('OK,  e-mail de ajuda enviado com sucesso: ', res);
        this.loading(false);
        this.presentConfirm('<img src="assets/img/icons/icon-success.png">', this.message);
      })
      .catch(err => {
        this.message = "ERRO ao enviar mensagem de ajuda.";
        console.error('ERROR ao enviar e-mail de ajuda:', err)
        this.loading(false);
        this.presentConfirm('<img src="assets/img/icons/icon-error.png">', this.message);

      });

  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: ""+this.message,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "OK",
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });

    toast.present();
  }

  loading(show) {
    let loading = this.loadingCtrl.create({
      spinner: "hide",
      content: `
      <span
        class="spinner
        spinner-large
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2500
    });

    loading.onDidDismiss(() => {});
    if (show === true) {
      loading.present();
    }
    console.log("chamei loader: ", show);
  }

  presentConfirm(icon, msg) {
    if (icon && msg) {
      let alert = this.alertCtrl.create({
        title: "Solaris",
        message: ''+icon+'<br> <p>'+ msg+'</p>',
        buttons: [
          {
            text: "OK",
            role: "cancel",
            handler: () => {
              this.resetHelpForm();
            }
          },

        ],
        cssClass: "modal-with-icon"
      });
      alert.present();
    }
  }

  resetHelpForm(){
    this.showValidation = false;
    // this.data = {
    //   name: '',
    //   email: '',
    //   phone: '',
    //   message: ''

    // }
    this.message = '';
  }

}
