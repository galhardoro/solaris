import { TranslateService } from '@ngx-translate/core';


import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';



import { Api } from '../../providers/api/api';

import { TabAffiliatedList, TabAffiliatedMap } from './../pages';



@IonicPage()
@Component({
  selector: 'page-affiliated',
  templateUrl: 'affiliated.html',
})
export class AffiliatedPage {
  message: any;
  name: any;

  tabAffiliatedList: any = TabAffiliatedList;
  tabAffiliatedMap: any = TabAffiliatedMap;

  tabTitleList: any =  "LISTA";
  tabTitleMap: "MAPA";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public translateService: TranslateService,
    public api: Api) {

      this.presentLoadingCustom();
      // this.tabTitleList = "LISTA";
      // this.tabTitleMap = "MAPA";

      translateService.get(['LISTA', 'MAPA']).subscribe(values => {
        this.tabTitleList = values['LISTA'];
        this.tabTitleMap = values['MAPA'];        
      });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AffiliatedPage');
    this.presentLoadingCustom();
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });
  
    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });
  }

 
    
}
