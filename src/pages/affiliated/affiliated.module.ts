import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AffiliatedPage } from './affiliated';

@NgModule({
  declarations: [
    AffiliatedPage,
  ],
  imports: [
    IonicPageModule.forChild(AffiliatedPage),
    TranslateModule.forChild()
  ],
  exports: [
    AffiliatedPage
  ]
})
export class AffiliatedPageModule {}
