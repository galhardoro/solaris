import { Item } from './../../models/item';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Api } from '../../providers/providers';
import { ProductImageURL } from '../../pages/pages';
import { Categories } from '../../models/products/categories';

// import { SplashScreen } from '@ionic-native/splash-screen';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  banners: any[];
  prodcategories: Categories[] = new Array<Categories>();
  categories: number[] = [1, 2, 3, 4, 5, 6, 7, 12, 13, 14];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public api: Api) {
    this.initializeBanner();
    this.initializeProductsCategories();
  }


  initializeBanner() {
    this.api.get('api/mobile/banners').then(res => {
      // res['data'].forEach((item) => {
      //   item.image = encodeURI(ProductImageURL + item.image);
      // });
      //<p>Locação e Venda de Plataformas Elevatória para todo Brasil. Ligue: <span class="LrzXr zdqRlf kno-fv"><a class="fl r-iAgyyCrDCWY8" href="https://www.google.com.br/search?ei=oF3zWr7sI4rFwASbiKTYAw&amp;q=solaris+equipamentos&amp;oq=solaris+e&amp;gs_l=psy-ab.1.0.0l10.3214.3684.0.4616.2.2.0.0.0.0.100.194.1j1.2.0....0...1c.1.64.psy-ab..0.2.192...0i67k1.0.45SeBpcXwOk#" data-number="+558007020010" data-pstn-out-call-url="" title="Ligar pelo Hangouts" data-rtid="iAgyyCrDCWY8" data-ved="2ahUKEwiIr_W9v_naAhXEDpAKHc5aDDgQkAgoADAVegUIABC7AQ">0800 702 0010</a></span><br></p>
      //this.banners = res['data'];
      this.banners = new Array<any>();
      res['data'].forEach(element => {
        element.body = this.removeHTML(element.body);
        this.banners.push(element);
        console.info(element);
      });
      console.log(this.banners);
    })
  }

  initializeProductsCategories() {
    this.api.get('api/mobile/products/categories').then((res: Array<Categories>) => {
      //item.image = encodeURI(ProductImageURL + item.image);
     
      this.prodcategories.push()
      //this.prodcategories = res['data'];
      console.info(this.prodcategories);
    });
  }

  // productDetails(pcat): void {
  //   console.log(pcat);
  // }

  productDetails(pcat: Item) {
    // TODO remover após ajuste da API
    pcat.image = "http://admin.solarisbrasil.com.br/uploads/documents/Manipulador-SkyTrak-10042-1-min.png"

    this.navCtrl.push('ItemBannerDetailPage', {
      item: pcat
    });
    console.log('produto a exibir o detalhe \n name: ', pcat.name, '\n product: ', pcat);

  }

  productCartAdd(pcat): void {
    console.log(pcat);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.presentLoadingCustom();
  }

  goToSimulator() {
    this.navCtrl.push('SimulatorPage')
  }

  goToCatalog(){
    this.navCtrl.push('CatalogPage')
  }

  goToHelp() {
    this.navCtrl.push('HelpPage')
  }


  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span
        class="spinner
        spinner-large
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 3000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    loading.present();
  }

  removeHTML(str: String) {
    return str.replace(/<.*?>/g, '');
  }
}
