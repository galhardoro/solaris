import { EmailComposer } from '@ionic-native/email-composer';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { CartItem } from '../../models/cartItem';
import { CartItems } from '../../providers/cart/cartItems';
// import { CartItems } from './../../mocks/providers/cartItems';
import { Api } from '../../providers/api/api';
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  cartItems: any[];
  private message: String = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public api: Api,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private emailComposer: EmailComposer,
    // public CartItems: CartItems,

  ) {
    this.loadProducts();
    this.presentLoadingCustom();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  loadProducts() {
    this.api.get('api/products').then(res => {
      this.cartItems = res["data"];
      this.cartItems.filter(function (item) {
        if (!item.image || item.image === null || item.image === "null") {
          // item.image = "assets/img/no-image.png" 
          item.image = "http://admin.solarisbrasil.com.br/uploads/documents/Manipulador-SkyTrak-10042-1-min.png"
        }
      });
      this.cartItems = this.filterProducts(11);
      // seminovos
      console.log('items do carrinho: ', this.cartItems);
    });
  }

  filterProducts(disponibility_id: number): any[] {
    var item_products = [];
    this.cartItems.filter(function (item) {
      if (!item.image || item.image === null || item.image === "null") {
        // item.image = "assets/img/no-image.png" 
        item.image = "http://admin.solarisbrasil.com.br/uploads/documents/Manipulador-SkyTrak-10042-1-min.png"
      }

      item.disponibility.find((item_diponibility) => {
        if (parseInt(item_diponibility.disponibility_id) === disponibility_id) {
          item_products.push(item);
        }
      });
    });
    return item_products;
  }


  deleteItem(item) {
    // this.cartItems.slice(1, item);
    console.log('produtc to delete: ', item.name, '\n', item);
  }

  goToCatalog() {
    this.navCtrl.push('CatalogPage');
  }

  closeQuote() {
    console.log('finalização cotação: ');
    this.presentModalCloseQuote();
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    loading.present();
  }

  presentModalCloseQuote() {
    let alert = this.alertCtrl.create({
      title: 'Finalizar cotação',
      inputs: [
        {
          name: 'name',
          placeholder: 'nome *'
        },
        {
          name: 'email',
          placeholder: 'e-mail*',
          type: 'email'
        },
        {
          name: 'telefone',
          placeholder: 'telefone*',
          type: 'tel'
        },
        {
          name: 'empresa',
          placeholder: 'empresa*',
          type: 'text'
        },
        {
          name: 'cnpj',
          placeholder: 'CNPJ*',
          type: 'text'
        },

      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'enviar',
          handler: data => {
            var quoteIsValid = this.validateQuote(
              data.name,
              data.email,
              data.telefone,
              data.empresa,
              data.cnpj
            );
            if (quoteIsValid) {
              this.sendEmail(
                data.name,
                data.email,
                data.telefone,
                data.empresa,
                data.cnpj
              )
              console.log('cotação enviada com sucesso!', data.name);
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  validateQuote(name, mail, fone, empresa, cnpj) {

    if (name == '') {
      this.presentToast("nome não pode ficar em branco");
    }

    if (mail == '') {
      this.presentToast("e-mail não pode ficar em branco");
    }

    if (fone == '') {
      this.presentToast("telefone não pode ficar em branco");
    }


    if (empresa == '') {
      this.presentToast("empresa não pode ficar em branco");
    }

    if (cnpj == '') {
      this.presentToast("cnpj não pode ficar em branco");
    }
    
    if(
      name !== '' &&
      mail !== '' &&
      fone !== '' &&
      empresa !== '' &&
      cnpj !== ''

    ){
      return true;
    } else {
      return false;
    }
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: "" + message,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "OK",
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });

    toast.present();
  }

  sendEmail(
    name,
    emailUser,
    telefone,
    empresa,
    cnpj
  ) {
    let email = {
      to: 'andersonlimahw@gmail.com',
      cc: 'galhardo.dn@gmail.com',
      attachments: [
        'file:http://www.solarisbrasil.com.br/src/img/logo-solaris.png'
      ],
      subject: 'AJUDA | SOLARIS',
      body: '<h3> Nova solicitação via aplicativo</h3> <br> <ul> <li>Nome: '+name+'</li> <li>E-mail: '+emailUser+' </li> <li>Telefone: '+telefone+' </li> <li>Empresa: '+empresa+' </li> <li>cnpj: '+cnpj+' </li> </ul>  <h4> Solaris APP</h4>',
      isHtml: true
    } 
    
    this.emailComposer.addAlias('gmail', 'com.google.android.gm');
    
    this.emailComposer.open(email, {
      app: 'gmail'
    })
    .then(() =>  {
      this.message = 'Por favor confira os dados e envie o e-mail, obrigado '+name+'.';
      this.presentToast(this.message);
    }).catch((error) => {
      this.message = 'Erro ao acessar o aplicativo de e-mail.';
      this.presentToast(this.message);
    });
  }
}
