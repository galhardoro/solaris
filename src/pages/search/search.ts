import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, LoadingController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';
import { Api } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  currentItems: any[];
  novos: any[];
  data: any[];

  constructor(public navCtrl: NavController,
    public items: Items,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public api: Api) {
    this.loadProducts();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    this.presentLoadingCustom();
  }


  loadProducts() {
    this.api.get('api/products').then((res) => {      
      this.currentItems = res['data'];
      this.novos = this.filterProducts(4);
      this.presentLoadingCustom();

      if (this.novos){
        this.currentItems = this.novos;
        console.log("produtos novos:", this.novos, 'currentItems: ', this.currentItems);
      } else {
        console.error(' não troxe produtos novos: ', this.novos)
      }

    });
  }

  filterProducts(disponibility_id: number) : any[] {
    var item_products = [];
    this.currentItems.filter(function (item) {
      if (!item.image || item.image === null || item.image === "null") {
        item.image = "assets/img/no-image.png" 
        // item.image = "http://admin.solarisbrasil.com.br/uploads/documents/Manipulador-SkyTrak-10042-1-min.png"
      }

      item.disponibility.find((item_diponibility) => {
        if (parseInt(item_diponibility.disponibility_id) === disponibility_id) {
          item_products.push(item);
        }
      });
    });
    return item_products;
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.currentItems.push(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.currentItems.slice(1,item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });
  
    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });
  
    loading.present();
  }

}
