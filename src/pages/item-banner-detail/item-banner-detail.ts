import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Items } from "../../mocks/providers/items";

@IonicPage()
@Component({
  selector: 'page-item-banner-detail',
  templateUrl: 'item-banner-detail.html'
})

export class ItemBannerDetailPage {
  item: any;
  
  @ViewChild('slides') slider: any;

  constructor(
    public navCtrl: NavController, 
    navParams: NavParams, 
    public loadingCtrl: LoadingController,
    items: Items) {
    this.item = navParams.get('item') || items.defaultItem;
  }

  ionViewDidLoad() {
    this.presentLoadingCustom();
  }


  prevSlide() {
    this.slider.slidePrev();
  }

  nextSlide() {
    this.slider.slideNext();
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });
  
    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });
  
    loading.present();
  }

}
