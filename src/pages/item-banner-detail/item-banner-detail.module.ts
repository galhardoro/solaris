import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ItemBannerDetailPage } from './item-banner-detail';

@NgModule({
  declarations: [
    ItemBannerDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ItemBannerDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    ItemBannerDetailPage
  ]
})
export class ItemBannerDetailPageModule {}
