// // GMAPS
// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker
// } from '@ionic-native/google-maps';


import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { Api } from '../../providers/api/api';

declare var google;

@IonicPage()
@Component({
  selector: 'page-affiliated-map',
  templateUrl: 'affiliated-map.html',
})
export class AffiliatedMapPage {

  @ViewChild('map_canvas') mapElement: ElementRef;

  platform: any;
  // GMAPS
  //map: GoogleMap;
  map: any;
  bounds: any;
  // FIM GMAPS
  message: any;
  name: any;

  citiesSelected: any[];
  affiliateds: any[];

  state: any;
  states: any;

  city: any;
  cities: any;

  tabTitleList: any = "LISTA";
  tabTitleMap: any = "MAPA";

  defaultLat: any;
  degaultLog: any;



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public api: Api) {

    this.bounds = new google.maps.LatLngBounds();

    this.listAffiliates();
    this.presentLoadingCustom();
    this.tabTitleList = "LISTA";
    this.tabTitleMap = "MAPA"
  }

  ionViewDidLoad() {
    this.initializeMap();
  }

  listAffiliates(): void {
    var affiliatedsList = [];
    this.api.get('api/mobile/affiliated').then(res => {

      console.log('filiais: ', res["data"]);
      if (res && res["data"]) {
        res["data"].forEach(elem => {
          var affiliated = {
            id: elem.id,
            active: elem.active,
            name: elem.name,
            description: elem.description,
            address: elem.address,
            phone: elem.phone,
            uf: elem.uf,
            lat: elem.lat,
            lon: elem.lon,
            image: elem.image
          }

          affiliatedsList.push(affiliated);
          this.defaultLat = elem.lat;
          this.degaultLog = elem.lon;

          this.loadMap(parseFloat(elem.lat), parseFloat(elem.lon), elem);
        });

        this.affiliateds = affiliatedsList;
        var _states = [];
        var _cities = [];

        affiliatedsList.forEach((value, index) => {
          let name_state_city: any[] = value.name.replace(" ", "").replace(" ", "").split("/");

          // console.log('index: ', index, 'value: ', value);

          var state = {
            uf: value.uf,
            name: name_state_city[1]
          }
          _states.push(state);

          // console.log('states: ', _states);

          var city = {
            uf: value.uf,
            name: name_state_city[0]
          }
          _cities.push(city);
          //  console.log('cities: ', _cities)

        });
        this.states = _states;
        this.cities = _cities;

        //Centraliza o mapa de acordo com a quantidade de bounds/marcadores que tem no maps.
        this.map.fitBounds(this.bounds);

      } else {
        console.error('retorno inválido: ', res, res["data"]);
      }


    }).catch(error => {
      console.error('error ao obter retorno da API de filiais ', error);
    });
  }

  listCitiesFromUf(codUf: string) {
    this.citiesSelected = this.cities.filter((citi) => {
      return (citi.stateUFId == codUf)
    });
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span
        class="spinner
        spinner-large
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });
  }

  showAffiliatedDetail(affiliated) {
    if (affiliated) {
      this.name = affiliated.name;

      this.message =
        // 'Nome <p>'+this.name+'</p>'+
        'Descrição ' + affiliated.description + '' +
        'Endereço <p>' + affiliated.address + '</p>' +
        'Telefone <p>' + affiliated.phone + '</p>'
    } else {
      console.error('naõ foi encontrada a filial');
    }

    let alert = this.alertCtrl.create({
      title: '' + this.name,
      subTitle: '' + this.message,
      buttons: ['FECHAR'],
      cssClass: 'alert-affiliated'
    });
    alert.present();
  }

  initializeMap() {

    let latLng = new google.maps.LatLng(-23.533773, -46.625290);

    let mapOptions = {
      center: latLng,
      zoom: 5,
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      scaleControl: false,
      streetViewControl: false,
      streetViewControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      }
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  }

  // GMAPS
  loadMap(lat, lng, affiliated) {
    let latLng = new google.maps.LatLng(lat, lng);
    this.addMarker(latLng, affiliated);

  }

  addMarker(LatLng, affiliated) {

    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: LatLng
    });

    this.addInfoWindow(marker, affiliated);

    this.bounds.extend(LatLng);

  }

  addInfoWindow(marker, content_obj) {

    var content = '<div id="iw-container">' +
      '<div class="iw-title">' + content_obj.name + '</div>' +
      '<div class="iw-content">' +
      '<div class="iw-subTitle">Informações Gerais:</div>' +
      '<p>' + content_obj.description + '</p>' +
      '<p>' + content_obj.address + '<br>' +
      '<div class="iw-subTitle">Contatos:</div>' +
      'Tel:. ' + content_obj.phone + '</p>' +
      '</div>' +
      '<div class="iw-bottom-gradient"></div>' +
      '</div>';

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }
}
