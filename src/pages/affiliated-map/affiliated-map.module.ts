import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AffiliatedMapPage } from './affiliated-map';

@NgModule({
  declarations: [
    AffiliatedMapPage,
  ],
  imports: [
    IonicPageModule.forChild(AffiliatedMapPage),
  ],
  exports: [
    AffiliatedMapPage
  ]
})
export class AffiliatedMapPageModule {}
