import { Network } from "@ionic-native/network";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Nav,
  LoadingController
} from "ionic-angular";

/**
 * Generated class for the ErrorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-error",
  templateUrl: "error.html"
})
export class ErrorPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public nav: Nav,
    private network: Network,
    private loadingCtrl: LoadingController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad ErrorPage");
  }

  tryAgain() {
    this.presentLoadingCustom();
    var connected = false;
    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log("network connected!");
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.

      connected = true;
      if (connected) {
        
        console.log(
          "conexão com a internet restabelecida: ",
          this.network,
          "connectSubscription: ",
          connectSubscription
        );
        this.nav.setRoot("HomePage");
        // stop connect watch
        connectSubscription.unsubscribe();
      } else {
        console.log('sem conexão: ', this.network);
      }
    });
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: "hide",
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 3000
    });

    loading.onDidDismiss(() => {
      console.log("Dismissed loading");
    });

    loading.present();
  }
}
