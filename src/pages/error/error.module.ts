import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ErrorPage } from './error';

@NgModule({
  declarations: [
    ErrorPage,
  ],
  imports: [
    IonicPageModule.forChild(ErrorPage),
    TranslateModule.forChild()
  ],
  exports: [
    ErrorPage
  ]
})
export class ErrorPageModule {}
