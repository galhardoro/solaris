import { EmailComposer } from '@ionic-native/email-composer';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private emailComposer: EmailComposer,
    private callNumber: CallNumber
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  goToAbout() {
    this.navCtrl.push('AboutPage')
  }

  sendEmail() {
    let email = {
      to: 'noreply@solarisbrasil.com.br',
      cc: 'atendimento@solarisbrasil.com.br',
      subject: 'CONTATO | SOLARIS',
      body: '<h4> Solaris APP</h4>',
      isHtml: true
    } 
    
    this.emailComposer.addAlias('gmail', 'com.google.android.gm');
    
    this.emailComposer.open(email, {
      app: 'gmail'
    })
    .then(() =>  {
      console.log('sendMail: OK');
    }).catch((error) => {
      console.error('sendMail: OK', error);
    });
  }
  
  call0800(number){
    this.callNumber.callNumber(number, true)
    .then(() => console.log('Launched dialer!', number))
    .catch(() => console.error('Error launching dialer', number));
  }
 


}
