import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AffiliatedsPage } from './affiliateds';

@NgModule({
  declarations: [
    AffiliatedsPage,
  ],
  imports: [
    IonicPageModule.forChild(AffiliatedsPage),
  ]
})
export class AffiliatedsPageModule {}
