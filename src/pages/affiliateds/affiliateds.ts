import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the AffiliatedsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-affiliateds',
  templateUrl: 'affiliateds.html'
})
export class AffiliatedsPage {

  affiliatedListRoot = 'AffiliatedListPage'
  affiliatedMapRoot = 'AffiliatedMapPage'


  constructor(public navCtrl: NavController) {}

}
