import { Products } from './../../models/products/products';
import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, LoadingController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';
import { Api } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Products[];
  locacao: any[];
  data: any[];

  constructor(public navCtrl: NavController,
    public items: Items,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public api: Api) {
    
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    this.presentLoadingCustom();
    this.loadProducts(1, 20, 0);
  }


  loadProducts(page: number, pageSize: number, categoryId: number) {
    this.api.get(`api/mobile/products/${page}/${pageSize}/${categoryId}`).then((res: Products[]) => {
      this.currentItems = res;
      this.locacao = this.currentItems;
      this.presentLoadingCustom();

      if (this.locacao) {
        this.currentItems = this.locacao;
        console.log("produtos para locação:", this.locacao, 'currentItems: ', this.currentItems);
      } else {

        console.error("produtos para locação:", this.locacao, 'currentItems: ', this.currentItems);
      }

    });
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.currentItems.push(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.currentItems.slice(1, item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    loading.present();
  }


}
