import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { CallPage } from './call';

@NgModule({
  declarations: [
    CallPage,
  ],
  imports: [
    IonicPageModule.forChild(CallPage),
    TranslateModule.forChild()
  ],
  exports: [
    CallPage
  ]
})
export class CallPageModule {}
