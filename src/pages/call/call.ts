import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";

import { HelpFormService } from "./../../services/helpform";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  AlertController
} from "ionic-angular";

// import { Api } from '../../providers/api/api';
import { SolarisApi } from "../../providers/api/solarisapi";

/**
 * Generated class for the CallPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-call",
  templateUrl: "call.html"
})
export class CallPage {
  // navegação
  step: any = 0;
  stepBuble: any;
  locked: Boolean = false;
  lockedMessage: String = "";
  typeMessage: String = "";
  generalLoading: Boolean = false;

  //forms
  serieNumber: any;
  private data: any;
  fileSelected: any;

  secondStepValid: Boolean = false;
  thirdStepValid: Boolean = false;

  private isSubmitted: Boolean = false;
  private message: String = "";

  // solarisApi:
  // dados do chamado
  token: any;
  productLineId: any;
  category: any;
  InternalNumber: any;

  problems: any[]; // lista de problemas
  descriptionProblem: any; // descrição do problema =  ng-model da lista
  anotherProblemDescription: String = ""; // descrição do problema quando for outros.
  showAnotherProblem: Boolean = false;
  placeholderProblems: String = "Selecione...";
  // ion-select
  selectOptions = {
    title: "Problemas",
    subTitle: "Selecione um problema",
    mode: "md"
  };

  reqOpts = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private formData: HelpFormService,
    private toastCtrl: ToastController,
    private transfer: FileTransfer,
    private alertCtrl: AlertController,
    public solarisApi: SolarisApi // private file: File
  ) {
    this.formData = formData;
    this.data = {
      name: "",
      email: "",
      phone: "",
      message: ""
    };
    this.getToken();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CallPage");
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: "hide",
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 1000
    });

    loading.onDidDismiss(() => {
      console.log("Dismissed loading");
    });

    loading.present();
  }

  nextStep() {
    if (this.serieNumber && this.step === 0) {
      this.findNumber(this.serieNumber);
    } else if (this.step === 0) {
      this.presentToast("Por favor digite um número interno ou o de série.");
    }
    if (this.step == 2) {
      this.step = 2;
    }
  }
  prevStep() {
    this.step -= 1;
    if (this.step <= 0) {
      this.step = 0;
    }
    this.secondStepValid = false;
  }

  onSubmit(myForm) {
    this.isSubmitted = true;
    console.log("onSubmit");
    console.log(myForm);

    if (myForm.valid) {
      this.formData.name = this.data.name;
      this.formData.email = this.data.email;
      this.formData.phone = this.data.phone;

      this.message = "Formulário válido."; // toast
      this.presentToast(this.message);     
      this.findProblem(this.productLineId, this.token);
      this.secondStepValid = true;
      this.step += 1;
    } else {
      console.error("formulário inválido, ", myForm);
      this.message = "Por favor verifique todos os campos.";
      this.presentToast(this.message);
    }
  }
  noSubmit(e) {
    e.preventDefault();
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: "" + message,
      duration: 3000,
      position: "top",
      showCloseButton: true,
      closeButtonText: "OK",
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });

    toast.present();
  }

  uploadFile() {
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: "file",
      fileName: "name.jpg",
      chunkedMode: false,
      mimeType: "image/jpg",
      headers: {
        token:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
      }
    };

    fileTransfer.upload("/", "https://httpbin.org/post", options).then(
      data => {
        console.log("OK upload data ", data);
        this.fileSelected = data;
        alert("upload ok");
      },
      err => {
        console.error("ERRO upload data ", err);
        alert("upload ERRO");
      }
    );
  }

  send() {
    this.step = 2;
    this.openCase();
    this.presentLoadingCustom();
  }

  presentConfirm(icon, response, msg) {
    if (response && response === "OK") {
      let alert = this.alertCtrl.create({
        title: "Solaris",
        message: "" + icon + "<br> <p>" + msg + "</p>",
        buttons: [
          {
            text: "Cancelar",
            role: "cancel",
            handler: () => {
              console.log("Cancelado.");
            }
          },

          {
            text: "Avançar",
            handler: () => {
              console.log("Prosseguir com chamado.");
              this.step += 1;
            }
          }
        ],
        cssClass: "modal-with-icon"
      });
      alert.present();
    } else if (response && response === "ERRO") {
      let alert = this.alertCtrl.create({
        title: "Solaris",
        message: "" + icon + "<br> <p>" + msg + "</p>",

        buttons: [
          {
            text: "Ok",
            role: "cancel",
            handler: () => {
              console.log("resetar chamado");
              this.resetCalled();
            }
          }
        ],
        cssClass: "modal-with-icon"
      });
      alert.present();
    }
  }

  successOnCall(icon, response, msg) {
    if (response && response === "OK") {
      let alert = this.alertCtrl.create({
        title: "Solaris",
        message: "" + icon + "<br> <p>" + msg + "</p>",
        buttons: [
          {
            text: "OK",
            handler: () => {
              console.log("Finalizado com sucesso.");
              this.resetCalled();
            }
          }
        ],
        cssClass: "modal-with-icon"
      });
      alert.present();
    } else if (response && response === "ERRO") {
      let alert = this.alertCtrl.create({
        title: "Erro ao  abrir chamado!",
        message: "" + icon + "<br> <p>" + msg + "</p>",
        buttons: [
          {
            text: "Ok",
            role: "cancel",
            handler: () => {
              console.log("Prosseguir com chamado.");
              this.resetCalled();
            }
          }
        ],
        cssClass: "modal-with-icon"
      });
      alert.present();
    }
  }
  resetCalled() {
    // limpa os dados do chamado:
    this.data.name = null;
    this.data.phone = null;
    this.data.email = null;
    this.descriptionProblem = null;
    this.InternalNumber = null;
    this.thirdStepValid = false;
    this.step = 0;
    this.serieNumber = null;
    this.secondStepValid = false;
    this.locked = false;
    this.lockedMessage = "";
    this.typeMessage = null;
    this.isSubmitted = false;
    this.anotherProblemDescription = null;
    this.showAnotherProblem = false;
    
    // pega novo token
    this.generalLoading = false;
    this.getToken();
  }
  // 1 - pegar token
  getToken() {
    this.loading(true, 3000);
    var body = {
      userapp: "APPSOLARIS_01",
      password: "123"
    };

    this.solarisApi
      .getToken(body)
      .then(res => {
        res["value"].forEach(item => {
          this.token = item.tokenUser;
          console.log("GET TOKEN, token: ", this.token);
          this.loading(false, 0);
        });
      })
      .catch(err => {
        console.error(
          "GET TOKEN, token,",
          this.token,
          "\n body: ",
          body,
          "options: ",
          this.reqOpts
        );
        this.loading(false, 0);
      });

    // alert('TOKEN: '+this.token);
  }

  // 2 - pesquisar número de série findNumber

  findNumber(number) {
    this.loading(true, 3000);
    if (number) {
      this.InternalNumber = number;
      // this.getToken();

      if (this.token) {
        var body = {
          InternalNumber: this.InternalNumber,
          token: this.token
        };
      }

      this.solarisApi
        .findFleetByIntenalNumber(body)
        .then(res => {
          var description = res["description"];

          res["value"].forEach(item => {
            this.productLineId = item.iD_LINHAPRODUTO;
            this.category = item.familiafabricante;

            this.validateResult(item, description);

            console.log("Linha de produto encontrada: ", this.productLineId);
            this.loading(false, 0);
          });
          console.log(
            "Equipamento identificado",
            number,
            "findFleetByIntenalNumber response : ",
            res
          );
        })
        .catch(err => {
          this.presentConfirm(
            '<img src="assets/img/icons/icon-error.png">',
            "ERRO",
            err.message
          );
          console.error("findFleetByIntenalNumber response : ", err);
          this.loading(false, 0);
        });
    }
  }
  // 3 - encontrar problema baseado na linha de produto
  findProblem(productLineId, token) {
    // this.getToken();
    this.loading(true, 5000);
    var body = {
      LineProduct: productLineId,
      token: token
    };
    // tratar o nextstep;
    this.solarisApi
      .findProblemFleet(body)
      .then(res => {
        this.problems = res["value"];
        console.log(
          "OK findProblem: ",
          "\nbody",
          body,
          "\n res: ",
          res["value"],
          "\n problems: ",
          this.problems
        );
        this.loading(false, 0);
      })
      .catch(err => {
        console.error("ERRO: findProblem: ", err, "\nbody: ", body);
        this.loading(false, 0);
      });
  }
  // abrir chamado
  openCase() {

    if(this.anotherProblemDescription && this.anotherProblemDescription !== ""){
      this.descriptionProblem.push(this.anotherProblemDescription);
    }

    let problems = this.descriptionProblem.join(',');
    console.log('problems: ', problems);

    this.loading(true, 9000);
    this.generalLoading = true;
    var body = {
      token: this.token,
      Name: this.data.name,
      Phone: this.data.phone,
      Email: this.data.email,
      DescriptionProblem: problems,      
      InternalNumber: this.InternalNumber
    };
    var _msg = "- - -";

    this.solarisApi
      .openCases(body)
      .then(res => {
        var answer = res["answer"];
        var description = res["description"];

        if (answer == 200 || answer == 201 || answer == 204) {
          res["value"].forEach(item => {
            this.validateResultCase(item, description);
            this.generalLoading = false;
            console.log("answer, OK: ", answer);
            console.log(
              "OK openCase: ",
              "\nbody",
              body,
              "\n res: ",
              res["value"]
            );
          });
        } else {
          this.presentConfirm(
            '<img src="assets/img/icons/icon-error.png">',
            "ERRO",
            _msg
          );
          this.generalLoading = false;
          console.log("answer, ERRO: ", answer);
        }
        this.loading(false, 0);
      })
      .catch(err => {
        _msg = err.message;
        this.successOnCall(
          '<img src="assets/img/icons/icon-error.png">',
          "ERRO",
          _msg
        );
        console.error("ERRO: openCase: ", err, "\nbody: ", body);
        this.generalLoading = false;
        this.loading(false, 0);
      });
  }

  loading(show, duration) {
    let loading = this.loadingCtrl.create({
      spinner: "hide",
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: duration
    });

    loading.onDidDismiss(() => {});
    if (show === true) {
      loading.present();
      console.log("chamei loader: ", show);
    }
  }

  validateResult(item, description) {
    // validação de retorno e status
    if (item.lock === "1") {
      this.locked = true;
      this.lockedMessage = item.message;
      console.log("locked: ", this.locked, item.lock);
    } else {
      this.locked = false;
      this.lockedMessage = "";
      console.log("locked: ", this.locked, item.lock);
    }

    let msg = "- - -";
    if (item.message && item.message !== "") {
      msg = item.message;
    } else if (description && description !== "") {
      msg = description;
    }

    if (item.type_Message == "1") {
      this.presentConfirm(
        '<img src="assets/img/icons/icon-success.png">',
        "OK",
        msg
      );
    } else if (item.type_Message == "2") {
      this.presentConfirm(
        '<img src="assets/img/icons/icon-error.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "3") {
      this.presentConfirm(
        '<img src="assets/img/icons/icon-alert.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "4") {
      this.presentConfirm(
        '<img src="assets/img/icons/icon-info.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "5") {
      this.presentConfirm(
        '<img src="assets/img/icons/icon-question.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "" && item.numberCase !== "" || item.type_Message == null &&  item.numberCase !== "") {
      this.presentConfirm(
        '<img src="assets/img/icons/icon-success.png">',
        "OK",
        msg
      );
    }
  }

  validateResultCase(item, description) {
    // validação de retorno e status
    if (item.lock === "1") {
      this.locked = true;
      this.lockedMessage = item.message;
      console.log("locked: ", this.locked, item.lock);
    } else {
      this.locked = false;
      this.lockedMessage = "";
      console.log("locked: ", this.locked, item.lock);
    }

    let msg = "- - -";
    if (item.message && item.message !== "") {
      msg = item.message;
    } else if (description && description !== "") {
      msg = description;
    }

    if (item.type_Message == "1") {
      this.successOnCall(
        '<img src="assets/img/icons/icon-success.png">',
        "OK",
        msg
      );
    } else if (item.type_Message == "2") {
      this.successOnCall(
        '<img src="assets/img/icons/icon-error.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "3") {
      this.successOnCall(
        '<img src="assets/img/icons/icon-alert.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "4") {
      this.successOnCall(
        '<img src="assets/img/icons/icon-info.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "5") {
      this.successOnCall(
        '<img src="assets/img/icons/icon-question.png">',
        "ERRO",
        msg
      );
    } else if (item.type_Message == "" && item.numberCase !== "" || item.type_Message == null &&  item.numberCase !== "") {
      this.successOnCall(
        '<img src="assets/img/icons/icon-success.png">',
        "OK",
        msg
      );
    }
  }

  // select de problemas
  checkAnotherProblem(selectedValue: any) {
    let problems = selectedValue;
    let requiredAnotherProblemDescription = problems.find((problem)=>{
      let _showAnotherProblem = false;

      if(problem.toLowerCase() === "outros" || problem.toLowerCase() === "outro"){
        _showAnotherProblem = true;
      }

      this.showAnotherProblem = _showAnotherProblem;
      return _showAnotherProblem;
    });
    console.log('Selected problem', selectedValue, 'requiredAnotherProblemDescription: ', requiredAnotherProblemDescription);

  }
}
