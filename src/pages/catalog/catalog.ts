
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';

import { Tab1Root } from '../pages';
import { Tab2Root } from '../pages';
import { Tab3Root } from '../pages';


import { Item } from '../../models/item';
import { Items } from '../../providers/providers';
import { Api } from '../../providers/providers';
import { Products } from '../../models/products/products';
import { Categories } from '../../models/products/categories';

/**
 * Generated class for the CatalogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-catalog',
  templateUrl: 'catalog.html'
})
export class CatalogPage implements OnInit {


  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;


  tab1Title = "LOCAÇÃO";
  tab2Title = "NOVOS ";
  tab3Title = "SEMINOVOS";

  currentItems: Products[];
  prodcategories: Array<Categories> = new Array<Categories>();
  locacao: any[];
  data: any[];
  _loading: any;
  categorySelected: any = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public items: Items,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public api: Api
  ) {
  }

  ngOnInit(): void {
    this.presentLoadingCustom();
    this.loadProductsCategories();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    this.loadProducts(1, 200, 0);
  }

  loadProducts(page: number, pageSize: number, categoryId: number) {
    this.api.get(`api/mobile/products/${page}/${pageSize}/${categoryId}`).then((res: Products[]) => {
      this.currentItems = [];
      this.currentItems = res;
      this.locacao = this.currentItems;
      if (this.locacao) {
        this.currentItems = this.locacao;
        console.log("produtos para locação:", this.locacao, 'currentItems: ', this.currentItems);
      } else {
        console.error("produtos para locação:", this.locacao, 'currentItems: ', this.currentItems);
      }
    });
  }

  filterProducts(ev: any) {
    try {
      const val: string = ev.target.value;
      if (this.categorySelected != undefined || this.categorySelected > 0) {
        const categorySelected = this.categorySelected;
        this.api.get(`api/mobile/products/0/200/${categorySelected}`).then((res: Products[]) => {
          this.currentItems = [];
          this.currentItems = res;
          this.locacao = this.currentItems;
          if (this.locacao) {
            if (val && val.trim() != '' && val.trim().length > 3) {
              this.currentItems = this.locacao.filter((item: Products) => {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1)
                  || (item.name.toLowerCase() == val.toLowerCase())
                  || (String(item.metaTitle.toLowerCase()).includes(val.toLowerCase()));
              });
              console.log(this.currentItems);
            } else {
              this.currentItems = this.locacao;
            }
            console.log("produtos para locação:", this.locacao, 'currentItems: ', this.currentItems);
          } else {
            console.error("produtos para locação:", this.locacao, 'currentItems: ', this.currentItems);
          }
        });

      } else {
        if (val && val.trim() != '' && val.trim().length > 3) {
          this.currentItems = this.locacao.filter((item: Products) => {
            return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1)
              || (item.name.toLowerCase() == val.toLowerCase())
              || (String(item.metaTitle.toLowerCase()).includes(val.toLowerCase()));
          });
          console.log(this.currentItems);
        } else {
          this.loadProducts(1, 200, 0);
        }
      }

    } catch (error) {
      console.log(error);
    }
  }

  filterProductsWithCategory(category: string) {
    this.categorySelected = category;
    this.presentLoadingCustom();
    if (parseInt(category) > 0) {
      this.loadProducts(0, 200, parseInt(category));
    } else {
      this.loadProducts(0, 200, 0);
    }

  }

  loadProductsCategories() {
    this.api.get('api/mobile/products/categories').then((res: Array<Categories>) => {
      //item.image = encodeURI(ProductImageURL + item.image);
      this.prodcategories = res;
      var cat = new Categories();
      cat.id = "0";
      cat.parentId = "0";
      cat.title = "Todos os Produtos";
      this.prodcategories.unshift(cat);
      //this.prodcategories = res['data'];
      console.info(this.prodcategories);
      this._loading.dismiss();
    });
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.currentItems.push(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.currentItems.slice(1, item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

  presentLoadingCustom() {
    this._loading = null;
    this._loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 5000
    });

    this._loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    this._loading.present();
  }
}
