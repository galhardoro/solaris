import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { CatalogPage } from './catalog';

@NgModule({
  declarations: [
    CatalogPage,
  ],
  imports: [
    IonicPageModule.forChild(CatalogPage),
    TranslateModule.forChild()
  ],
  exports: [
    CatalogPage
  ]
})
export class CatalogPageModule {}
