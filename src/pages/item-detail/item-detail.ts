import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Items } from "../../mocks/providers/items";
import { Products } from '../../models/products/products';

import { ImageViewerController } from 'ionic-img-viewer';

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})

export class ItemDetailPage implements OnInit {

  item: Products;
  location: any;
  newSell: any;
  _imageViewerCtrl:ImageViewerController;

  @ViewChild('slides') slider: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private imageViewerCtrl: ImageViewerController,
    items: Items) {
      this._imageViewerCtrl = imageViewerCtrl;
  }

  ngOnInit(): void {
    this.item = this.navParams.get('item');
    console.log(this.item);
  }

  ionViewDidLoad() {
    this.presentLoadingCustom();
  }


  prevSlide() {
    this.slider.slidePrev();
  }

  nextSlide() {
    this.slider.slideNext();
  }

  addToCart() {
    this.presentConfirm();
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Produto adicionado com sucesso!',
      message: 'O produto foi adicionado ao carrinho.',
      buttons: [
        {
          text: 'Voltar',
          role: 'cancel',
          handler: () => {

            // go to cart
          }
        },
        {
          text: 'Ver Carrinho',
          handler: () => {
            this.goToCart();
            // go to cart
          }
        }
      ]
    });
    alert.present();
  }


  goToCart() {
    this.navCtrl.push('CartPage')
    console.log('goTo Cart');
  }

  presentLoadingCustom() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
      <span 
        class="spinner 
        spinner-large 
        spinner-primary
        spinner-slow">
      </span>`,
      duration: 2000
    });

    loading.onDidDismiss(() => {
      console.log('Dismissed loading');
    });

    loading.present();
  }

  openModal(currentImage) {
    const imageViewer = this._imageViewerCtrl.create(currentImage);
    imageViewer.present();
 
    //setTimeout(() => imageViewer.dismiss(), 1000);
    imageViewer.onDidDismiss(() => console.log('Viewer dismissed'));
  }

}
