import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SubsidiariesPage } from './subsidiaries';

@NgModule({
  declarations: [
    SubsidiariesPage
  ],
  imports: [
    IonicPageModule.forChild(SubsidiariesPage),
    TranslateModule.forChild()
  ],
  exports: [
    SubsidiariesPage
  ]
})
export class SubsidiariesPageModule {}
