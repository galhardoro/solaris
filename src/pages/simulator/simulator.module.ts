import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SimulatorPage } from './simulator';

@NgModule({
  declarations: [
    SimulatorPage,
  ],
  imports: [
    IonicPageModule.forChild(SimulatorPage),
    TranslateModule.forChild()
  ],
  exports: [
    SimulatorPage
  ]
})
export class SimulatorPageModule {}
