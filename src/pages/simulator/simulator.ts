import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Api } from '../../providers/providers';

/**
 * Generated class for the SimulatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-simulator',
  templateUrl: 'simulator.html',
})
export class SimulatorPage {
  
  // vars
  step: any;
  stepTitle: any;

  // set var to primesimulator {}  , {type: }
  primeSimulator: object;
  // primeSimulator attr
  primesimulator_id: any;
  type: any;
  category: any;
  environment: any;
  environmentCategory: any;
  application: any;
  height: any;
  model: any;
  utilizationData: any;
  needAcessory: any;
  acessories: any;

  //dropdown e item, Dados de utilização
  tensions: any[];
  tension: any;

  frequencies: any[];
  frequency: any;

  autonomies: any[];
  autonomy: any;
  // acessórios nevessários
  cables: any;
  qtm: any;
  qta: any;
  flames: any;
  transformers: any;
  externalTank: any;
  fireExtinguisher: any;
 


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public api: Api

  ) {
    //set vars
    this.step = 0;
    this.stepTitle = "Qual o tipo de situação você procura?";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SimulatorPage');
    this.step = 0;
    // dropdown de dados de utilização
    this.initializeTensions();
    this.initializeFrequencies();
    this.initializeAutonomies();
  }

  nextStep() {
    this.step += 1;
    if(this.step > 10) {
      this.step = 10;
    }
    this.updateStepTitle();
  }

  prevStep() {
    this.step -= 1
    if (this.step < 0) {
      this.step = 0;
    }
    this.updateStepTitle();
  }

  updateStepTitle() {

    if (this.step == 0) {
      this.stepTitle = "Qual o tipo de situação você procura?" ;
    } else if (this.step == 1) {
      this.stepTitle = "Escolha o item Categoria: " ;
    } else if (this.step == 2) {
      this.stepTitle = "Escolha o item Ambiente: " ;
    }
    else if (this.step == 3) {
      this.stepTitle = "Escolha o item Categoria de Ambiente: ";
    }
    else if (this.step == 4) {
      this.stepTitle = "Escolha o item Aplicação: ";
    }
    else if (this.step == 5) {  
      this.stepTitle = "Escolha o item Altura de Trabalho (m): ";
    }
    else if (this.step == 6) {
      this.stepTitle = "Informe o Modelo: ";
    } else if (this.step == 7) {

      this.stepTitle = "Dados de utilização:";
      this.initializeTensions();
      this.initializeFrequencies();
      this.initializeAutonomies();

    } else if (this.step == 8) {
      this.stepTitle = "Precisa de Acessórios?" ;
      
    } else if (this.step == 9) {
      this.stepTitle = "Selecione os acessórios que você precisa: " ;
      this.validateStep();
    } else if (this.step == 10) {
      this.stepTitle = "O produto ideal para você é " ;
    }

  }

  selectType(type) {
    this.type = type;
    console.log('type selected: ', this.type);
    this.nextStep();
  }

  selectCategory(category) {
    this.category = category;
    console.log('category selected: ', this.category);
    this.nextStep();
  }

  
  selectEnvironment(environment) {
    this.environment = environment;
    console.log('environment selected: ', this.environment);
    this.nextStep();
  }

  selectEnvironmentCategory(environmentCategory) {
    this.environmentCategory = environmentCategory;
    console.log('environmentCategory selected: ', this.environmentCategory);
    this.nextStep();
  }
  
  selectApplication(application) {
    this.application = application;
    console.log('application selected: ', this.application);
    this.nextStep();
  }

  selectHeight(height){
    this.height = height;
    console.log('height selected: ', this.height);
    this.nextStep();
  }

  selectModel(model){
    this.model = model;
    console.log('model selected: ', this.model);
    this.nextStep();
  }

  // Dados de utilização

  initializeTensions() {

    this.tensions = [
      {
        id: 1,
        name: "200V"
      },
      {
        id: 2,
        name: "380V"
      },
      {
        id: 3,
        name: "440V"
      }     

    ];
    
  }


  initializeFrequencies() {

    this.frequencies = [
      {
        id: 1,
        name: "50 Hz"
      },
      {
        id: 2,
        name: "60 Hz"
      }    

    ];

  }


  initializeAutonomies() {

    this.autonomies = [
      {
        id: 1,
        name: "8h/dia - 240/mês"
      },
      {
        id: 2,
        name: "12h/dia - 360h/mês"
      },
      {
        id: 3,
        name: "16h/dia - 480/mês"
      },
      {
        id: 4,
        name: "24/dia - 720/mês"
      },
      {
        id: 5,
        name: "Emergência"
      },
      {
        id: 6,
        name: "Horário de ponta"
      },

    ];

  }

  selectUtilizationData(){

    this.utilizationData = {
      tension: this.tension,
      frequency: this.frequency,
      autonomy: this.autonomy
    }
    console.log('utilizationData selected: ', this.utilizationData);
    this.nextStep();
  }

  selectNeedAcessory(needAcessory){
    this.needAcessory = needAcessory;    
    console.log('needAcessory selected: ', needAcessory);

    if(needAcessory == true){
      this.nextStep();
    } else {
      this.step +=2;
    }
  }
  selectAcessory(){   
    this.validateStep();
  }
  
  requestProposal(){
    // /api/primesimulator
    this.generadePrimesimulatorId();

    this.primeSimulator=  {
      // primeSimulator attr
      primesimulator_id: this.primesimulator_id,
      type: this.type,
      category: this.category,
      environment: this.environment,
      environmentCategory: this.environmentCategory,
      application: this.application,
      height: this.height,
      model: this.model,
      
      //dropdown e item, Dados de utilização
      tension: this.tension.name,
      frequency: this.frequency.name,      
      autonomy: this.autonomy.name,
      needAcessory: this.needAcessory,
      acessories: this.acessories
    
    }

    console.log('primeSimulator objeto a ser enviado para /api/primesimulator: ', 
         'JSON: ', JSON.stringify(this.primeSimulator));

    this.api.post('api/primesimulator', this.primeSimulator)
    .then(data => {
      
      this.presentToast("Proposta solicitada com sucesso !");
      console.log('data: ', data);
    }).catch(error => {
      this.presentToast("ERRO ao solicitar proposta!");
      console.error('ERRO ao solicitar proposta: ', error);
    })

    
    
  }
  
  generadePrimesimulatorId() {
    var date = new Date().getUTCMilliseconds();

    var _primesimulator_id = date + Math.random() * 1000 + 1;
    this.primesimulator_id = Math.floor(_primesimulator_id);

    return this.primesimulator_id;
  }


  // validações
  validateStep(){
    var validation = undefined;
    if(
      this.needAcessory = true &&
      this.cables == validation &&
      this.qtm == validation &&
      this.qta == validation &&
      this.flames == validation &&
      this.transformers == validation &&
      this.externalTank == validation &&
      this.fireExtinguisher == validation      
      
    ){
      //this.presentToast("não foi selecionado nenhum acessório deseja prosseguir?"); 
      this.presentConfirm();
    } else {
       var selectedAcessories = {
        needAcessory: this.needAcessory,
        cables: this.cables,
        qtm: this.qtm ,
        qta: this.qta,
        flames: this.flames,
        transformers: this.transformers,
        externalTank: this.externalTank,
        fireExtinguisher: this.fireExtinguisher
      }

      this.acessories = selectedAcessories;
      console.log('selectedAcessories: ', selectedAcessories);
      this.nextStep();
     
    }
  }
  
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: ""+message,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "SIM",
      dismissOnPageChange: true
    });
  
    toast.onDidDismiss(() => {
      // console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      message: 'Não foi selecionado nenhum acessório deseja proseguir?',
      buttons: [
        {
          text: 'NÃO',
          role: 'cancel',
          handler: () => {
            console.log('Cancelado.');
          }
        },
        {
          text: 'SIM',
          handler: () => {
            console.log('Prosseguir sem acessórios');
            this.nextStep()
          }
        }
      ]
    });
    alert.present();
  }
  

}
