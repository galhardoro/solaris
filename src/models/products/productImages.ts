export class ProductImage {
    productImageId: string;
    productId: string;
    imageName: string;
}