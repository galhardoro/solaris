import { ProductImage } from "./productImages";
import { ProductDetails } from "./productDetails";

export class Products {
  id: number;
  ProductId: number;
  name: string;
  metaTitle: string;
  body: string;
  image: string;
  geradorKva: string;
  categoryId: string;
  productDetails: ProductDetails[];
  productImages: ProductImage[];
}