export class Categories {
    id: string;
    parentId: string;
    title: string;
    description: string;
}