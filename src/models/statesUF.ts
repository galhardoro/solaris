export class StatesUf {
    constructor(id: string, uf: string) {
        this.id = id;
        this.uf = uf;
    }
    id: string;
    uf: string;
}