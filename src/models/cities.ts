export class Cities {
    constructor(stateUF: string, citiName: string) {
        this.stateUFId = stateUF;
        this.city = citiName;
    }
    
    /**
     * Codigo do UF relacionado.
     */
    stateUFId: string;
    /**
     * Nome da cidade
     */
    city: string;
}