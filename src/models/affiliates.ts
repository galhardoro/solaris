export class Affiliates {
   

    constructor(
        id:string,
        active: string,
        name: string,
        description: string,
        address: string,
        phone: string,
        uf: string,
        lat: string,
        lon: string,
        image: string
    ) {
        this.id = id;
        this.active =active;
        this.name = name;
        this.description = description;
        this.address = address;
        this.phone = phone
        this.uf = uf;
        this.lat = lat;
        this.lon = lon;
        this.image = image;
    }

    id: string;
    active: string;
    affiliated_id: string;
    name: string;
    description: string;
    address: string;
    phone: string;
    uf: string;
    lat: string;
    lon: string;
    cidade_show: string;
    image: string;
}