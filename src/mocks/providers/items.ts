import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  items: Item[] = [];

  defaultItem: any = {
    
      "id": "82",
      "product_id": "2",
      "lang": "pt",
      "name": "340AJ",
      "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance340AJ.jpg\"><br><img alt=\"\" src=\"http://104.131.183.107/uploads/documents/Plat-Articulada-340aj-2-min.jpg\">",
      "categories_id": "6",
      "media_id": "313",
      "slug": "340aj1",
      "image": "http://admin.solarisbrasil.com.br/uploads/documents/Manipulador-SkyTrak-10042-1-min.png",
      "search": " 340AJ  Lanças Articuladas  Plataformas Aéreas  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance340AJ.jpg\"><br><img alt=\"\" src=\"http://104.131.183.107/uploads/documents/Plat-Articulada-340aj-2-min.jpg\">  Externo - Diesel  Plano  Desnivelado  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  12,13  6,06  230  4.400  Diesel  5,52  1,93  2,00  4x4 ",
      "meta_description": "Com lança articulada e motor a diesel, a plataforma 340AJ atinge uma altura de 12,13 metros.",
      "meta_title": "Plataforma Aérea Articulada JLG 340AJ - Solaris Brasil",
      "gerador_kva": null,
      "category": "Lanças Articuladas",
      "disponibility": [
        {
          "product_id": "2",
          "id": "34",
          "disponibility_id": "4",
          "lang": "pt",
          "name": "Venda Novo",
          "url": "venda-de-equipamentos-novos"
        }
      ],
      "tags": [],
      "datasheet": [
        {
          "product_id": "2",
          "id": "82",
          "datasheet_id": "1",
          "lang": "pt",
          "name": "Marca",
          "filter": "1",
          "ordemGeradores": "1"
        },
        {
          "product_id": "2",
          "id": "61",
          "datasheet_id": "2",
          "lang": "pt",
          "name": "Altura de Trabalho (m)",
          "filter": "1",
          "ordemGeradores": "0"
        },
        {
          "product_id": "2",
          "id": "49",
          "datasheet_id": "3",
          "lang": "pt",
          "name": "Alcance Horizontal (m)",
          "filter": "1",
          "ordemGeradores": "0"
        },
        {
          "product_id": "2",
          "id": "64",
          "datasheet_id": "4",
          "lang": "pt",
          "name": "Capacidade (kg)",
          "filter": "0",
          "ordemGeradores": "0"
        },
        {
          "product_id": "2",
          "id": "91",
          "datasheet_id": "6",
          "lang": "pt",
          "name": "Peso Bruto (kg)",
          "filter": "0",
          "ordemGeradores": "11"
        },
        {
          "product_id": "2",
          "id": "88",
          "datasheet_id": "7",
          "lang": "pt",
          "name": "Motor",
          "filter": "0",
          "ordemGeradores": "0"
        },
        {
          "product_id": "2",
          "id": "73",
          "datasheet_id": "8",
          "lang": "pt",
          "name": "Comprimento",
          "filter": "0",
          "ordemGeradores": "8"
        },
        {
          "product_id": "2",
          "id": "76",
          "datasheet_id": "9",
          "lang": "pt",
          "name": "Largura",
          "filter": "0",
          "ordemGeradores": "9"
        },
        {
          "product_id": "2",
          "id": "52",
          "datasheet_id": "10",
          "lang": "pt",
          "name": "Altura",
          "filter": "0",
          "ordemGeradores": "10"
        },
        {
          "product_id": "2",
          "id": "109",
          "datasheet_id": "20",
          "lang": "pt",
          "name": "Tração",
          "filter": "0",
          "ordemGeradores": "0"
        }
      ],
      "images": []
    
  };


  constructor() {
    let items = [
      
      {
        "id": "82",
        "product_id": "2",
        "lang": "pt",
        "name": "340AJ",
        "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance340AJ.jpg\"><br><img alt=\"\" src=\"http://104.131.183.107/uploads/documents/Plat-Articulada-340aj-2-min.jpg\">",
        "categories_id": "6",
        "media_id": "313",
        "slug": "340aj1",
        "image": "assets/img/speakers/bear.jpg",
        "search": " 340AJ  Lanças Articuladas  Plataformas Aéreas  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance340AJ.jpg\"><br><img alt=\"\" src=\"http://104.131.183.107/uploads/documents/Plat-Articulada-340aj-2-min.jpg\">  Externo - Diesel  Plano  Desnivelado  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  12,13  6,06  230  4.400  Diesel  5,52  1,93  2,00  4x4 ",
        "meta_description": "Com lança articulada e motor a diesel, a plataforma 340AJ atinge uma altura de 12,13 metros.",
        "meta_title": "Plataforma Aérea Articulada JLG 340AJ - Solaris Brasil",
        "gerador_kva": null,
        "category": "Lanças Articuladas",
        "disponibility": [
          {
            "product_id": "2",
            "id": "34",
            "disponibility_id": "4",
            "lang": "pt",
            "name": "Venda Novo",
            "url": "venda-de-equipamentos-novos"
          }
        ],
        "tags": [],
        "datasheet": [
          {
            "product_id": "2",
            "id": "82",
            "datasheet_id": "1",
            "lang": "pt",
            "name": "Marca",
            "filter": "1",
            "ordemGeradores": "1"
          },
          {
            "product_id": "2",
            "id": "61",
            "datasheet_id": "2",
            "lang": "pt",
            "name": "Altura de Trabalho (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "2",
            "id": "49",
            "datasheet_id": "3",
            "lang": "pt",
            "name": "Alcance Horizontal (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "2",
            "id": "64",
            "datasheet_id": "4",
            "lang": "pt",
            "name": "Capacidade (kg)",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "2",
            "id": "91",
            "datasheet_id": "6",
            "lang": "pt",
            "name": "Peso Bruto (kg)",
            "filter": "0",
            "ordemGeradores": "11"
          },
          {
            "product_id": "2",
            "id": "88",
            "datasheet_id": "7",
            "lang": "pt",
            "name": "Motor",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "2",
            "id": "73",
            "datasheet_id": "8",
            "lang": "pt",
            "name": "Comprimento",
            "filter": "0",
            "ordemGeradores": "8"
          },
          {
            "product_id": "2",
            "id": "76",
            "datasheet_id": "9",
            "lang": "pt",
            "name": "Largura",
            "filter": "0",
            "ordemGeradores": "9"
          },
          {
            "product_id": "2",
            "id": "52",
            "datasheet_id": "10",
            "lang": "pt",
            "name": "Altura",
            "filter": "0",
            "ordemGeradores": "10"
          },
          {
            "product_id": "2",
            "id": "109",
            "datasheet_id": "20",
            "lang": "pt",
            "name": "Tração",
            "filter": "0",
            "ordemGeradores": "0"
          }
        ],
        "images": []
      },
      {
        "id": "83",
        "product_id": "3",
        "lang": "pt",
        "name": "450AJ Series II",
        "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso. De acordo com o modelo pode mover de 2 ou 3 pessoas.&nbsp;<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance450AJ.jpg\"><br>",
        "categories_id": "6",
        "media_id": "314",
        "slug": "450aj-series-ii1",
        "image": "assets/img/speakers/bear.jpg",
        "search": " 450AJ Series II  Lanças Articuladas  Plataformas Aéreas  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso. De acordo com o modelo pode mover de 2 ou 3 pessoas.&nbsp;<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance450AJ.jpg\"><br>  Externo - Diesel  Plano  Arenoso  Desnivelado  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  15,52  7,47  230  6.010  Diesel  6,71  2,34  2,29  4x2 ",
        "meta_description": "A altura da plataforma 450AJ é de 13,72 metros, com motor a diesel e maior velocidade de elevação.",
        "meta_title": "Plataforma Aérea Articulada JLG 450AJ Series ll - Solaris Brasil",
        "gerador_kva": null,
        "category": "Lanças Articuladas",
        "disponibility": [
          {
            "product_id": "3",
            "id": "34",
            "disponibility_id": "4",
            "lang": "pt",
            "name": "Venda Novo",
            "url": "venda-de-equipamentos-novos"
          }
        ],
        "tags": [],
        "datasheet": [
          {
            "product_id": "3",
            "id": "82",
            "datasheet_id": "1",
            "lang": "pt",
            "name": "Marca",
            "filter": "1",
            "ordemGeradores": "1"
          },
          {
            "product_id": "3",
            "id": "61",
            "datasheet_id": "2",
            "lang": "pt",
            "name": "Altura de Trabalho (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "3",
            "id": "49",
            "datasheet_id": "3",
            "lang": "pt",
            "name": "Alcance Horizontal (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "3",
            "id": "64",
            "datasheet_id": "4",
            "lang": "pt",
            "name": "Capacidade (kg)",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "3",
            "id": "91",
            "datasheet_id": "6",
            "lang": "pt",
            "name": "Peso Bruto (kg)",
            "filter": "0",
            "ordemGeradores": "11"
          },
          {
            "product_id": "3",
            "id": "88",
            "datasheet_id": "7",
            "lang": "pt",
            "name": "Motor",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "3",
            "id": "73",
            "datasheet_id": "8",
            "lang": "pt",
            "name": "Comprimento",
            "filter": "0",
            "ordemGeradores": "8"
          },
          {
            "product_id": "3",
            "id": "76",
            "datasheet_id": "9",
            "lang": "pt",
            "name": "Largura",
            "filter": "0",
            "ordemGeradores": "9"
          },
          {
            "product_id": "3",
            "id": "52",
            "datasheet_id": "10",
            "lang": "pt",
            "name": "Altura",
            "filter": "0",
            "ordemGeradores": "10"
          },
          {
            "product_id": "3",
            "id": "109",
            "datasheet_id": "20",
            "lang": "pt",
            "name": "Tração",
            "filter": "0",
            "ordemGeradores": "0"
          }
        ],
        "images": []
      },
      {
        "id": "84",
        "product_id": "4",
        "lang": "pt",
        "name": "450A Series II",
        "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i>",
        "categories_id": "6",
        "media_id": "315",
        "slug": "450a-series-ii1",
        "image": "assets/img/speakers/bear.jpg",
        "search": " 450A Series II  Lanças Articuladas  Plataformas Aéreas  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i>  Externo - Diesel  Plano  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  15,52  7,47  230  5.783  Diesel  6,68  2,34  2,29  4x2 ",
        "meta_description": "A plataforma 450A tem sua lança articulada, e atinge 15,52 metros de altura, alcançando pequenos locais.",
        "meta_title": "Plataforma Aérea Articulada JLG 450A Series ll - Solaris Brasil",
        "gerador_kva": null,
        "category": "Lanças Articuladas",
        "disponibility": [
          {
            "product_id": "4",
            "id": "34",
            "disponibility_id": "4",
            "lang": "pt",
            "name": "Venda Novo",
            "url": "venda-de-equipamentos-novos"
          }
        ],
        "tags": [],
        "datasheet": [
          {
            "product_id": "4",
            "id": "82",
            "datasheet_id": "1",
            "lang": "pt",
            "name": "Marca",
            "filter": "1",
            "ordemGeradores": "1"
          },
          {
            "product_id": "4",
            "id": "61",
            "datasheet_id": "2",
            "lang": "pt",
            "name": "Altura de Trabalho (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "4",
            "id": "49",
            "datasheet_id": "3",
            "lang": "pt",
            "name": "Alcance Horizontal (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "4",
            "id": "64",
            "datasheet_id": "4",
            "lang": "pt",
            "name": "Capacidade (kg)",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "4",
            "id": "91",
            "datasheet_id": "6",
            "lang": "pt",
            "name": "Peso Bruto (kg)",
            "filter": "0",
            "ordemGeradores": "11"
          },
          {
            "product_id": "4",
            "id": "88",
            "datasheet_id": "7",
            "lang": "pt",
            "name": "Motor",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "4",
            "id": "73",
            "datasheet_id": "8",
            "lang": "pt",
            "name": "Comprimento",
            "filter": "0",
            "ordemGeradores": "8"
          },
          {
            "product_id": "4",
            "id": "76",
            "datasheet_id": "9",
            "lang": "pt",
            "name": "Largura",
            "filter": "0",
            "ordemGeradores": "9"
          },
          {
            "product_id": "4",
            "id": "52",
            "datasheet_id": "10",
            "lang": "pt",
            "name": "Altura",
            "filter": "0",
            "ordemGeradores": "10"
          },
          {
            "product_id": "4",
            "id": "109",
            "datasheet_id": "20",
            "lang": "pt",
            "name": "Tração",
            "filter": "0",
            "ordemGeradores": "0"
          }
        ],
        "images": []
      },
      {
        "id": "85",
        "product_id": "5",
        "lang": "pt",
        "name": "600A",
        "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i>",
        "categories_id": "6",
        "media_id": "286",
        "slug": "600a1",
        "image": "assets/img/speakers/bear.jpg",
        "search": " 600A  Lanças Articuladas  Plataformas Aéreas  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i>  Externo - Diesel  Plano  Desnivelado  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  20,22  12,07  230  10.047  Diesel  8,10  2,49  2,54  4x4 ",
        "meta_description": "O modelo de plataforma 600A atinge uma altura de trabalho de 20,22 metros, com motor a diesel.",
        "meta_title": "Plataforma Aérea Articulada JLG 600A - Solaris Brasil",
        "gerador_kva": null,
        "category": "Lanças Articuladas",
        "disponibility": [
          {
            "product_id": "5",
            "id": "34",
            "disponibility_id": "4",
            "lang": "pt",
            "name": "Venda Novo",
            "url": "venda-de-equipamentos-novos"
          }
        ],
        "tags": [],
        "datasheet": [
          {
            "product_id": "5",
            "id": "82",
            "datasheet_id": "1",
            "lang": "pt",
            "name": "Marca",
            "filter": "1",
            "ordemGeradores": "1"
          },
          {
            "product_id": "5",
            "id": "61",
            "datasheet_id": "2",
            "lang": "pt",
            "name": "Altura de Trabalho (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "5",
            "id": "49",
            "datasheet_id": "3",
            "lang": "pt",
            "name": "Alcance Horizontal (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "5",
            "id": "64",
            "datasheet_id": "4",
            "lang": "pt",
            "name": "Capacidade (kg)",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "5",
            "id": "91",
            "datasheet_id": "6",
            "lang": "pt",
            "name": "Peso Bruto (kg)",
            "filter": "0",
            "ordemGeradores": "11"
          },
          {
            "product_id": "5",
            "id": "88",
            "datasheet_id": "7",
            "lang": "pt",
            "name": "Motor",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "5",
            "id": "73",
            "datasheet_id": "8",
            "lang": "pt",
            "name": "Comprimento",
            "filter": "0",
            "ordemGeradores": "8"
          },
          {
            "product_id": "5",
            "id": "76",
            "datasheet_id": "9",
            "lang": "pt",
            "name": "Largura",
            "filter": "0",
            "ordemGeradores": "9"
          },
          {
            "product_id": "5",
            "id": "52",
            "datasheet_id": "10",
            "lang": "pt",
            "name": "Altura",
            "filter": "0",
            "ordemGeradores": "10"
          },
          {
            "product_id": "5",
            "id": "109",
            "datasheet_id": "20",
            "lang": "pt",
            "name": "Tração",
            "filter": "0",
            "ordemGeradores": "0"
          }
        ],
        "images": []
      },
      {
        "id": "86",
        "product_id": "6",
        "lang": "pt",
        "name": "600AN Estreito",
        "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i>",
        "categories_id": "6",
        "media_id": "287",
        "slug": "600an-estreito1",
        "image": "assets/img/speakers/bear.jpg",
        "search": " 600AN Estreito  Lanças Articuladas  Plataformas Aéreas  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i>  Externo - Diesel  Plano  Desnivelado  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  20,22  12,07  230  10.931  Diesel  8,10  2,13  2,53  4x4 ",
        "meta_description": "Com seu chassi estreito, a plataforma 600AN permite trabalhar em áreas confinadas com até 20,22 metros de altura.",
        "meta_title": "Plataforma Aérea Articulada JLG 600AN Estreito - Solaris Brasil",
        "gerador_kva": null,
        "category": "Lanças Articuladas",
        "disponibility": [
          {
            "product_id": "6",
            "id": "34",
            "disponibility_id": "4",
            "lang": "pt",
            "name": "Venda Novo",
            "url": "venda-de-equipamentos-novos"
          }
        ],
        "tags": [],
        "datasheet": [
          {
            "product_id": "6",
            "id": "82",
            "datasheet_id": "1",
            "lang": "pt",
            "name": "Marca",
            "filter": "1",
            "ordemGeradores": "1"
          },
          {
            "product_id": "6",
            "id": "61",
            "datasheet_id": "2",
            "lang": "pt",
            "name": "Altura de Trabalho (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "6",
            "id": "49",
            "datasheet_id": "3",
            "lang": "pt",
            "name": "Alcance Horizontal (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "6",
            "id": "64",
            "datasheet_id": "4",
            "lang": "pt",
            "name": "Capacidade (kg)",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "6",
            "id": "91",
            "datasheet_id": "6",
            "lang": "pt",
            "name": "Peso Bruto (kg)",
            "filter": "0",
            "ordemGeradores": "11"
          },
          {
            "product_id": "6",
            "id": "88",
            "datasheet_id": "7",
            "lang": "pt",
            "name": "Motor",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "6",
            "id": "73",
            "datasheet_id": "8",
            "lang": "pt",
            "name": "Comprimento",
            "filter": "0",
            "ordemGeradores": "8"
          },
          {
            "product_id": "6",
            "id": "76",
            "datasheet_id": "9",
            "lang": "pt",
            "name": "Largura",
            "filter": "0",
            "ordemGeradores": "9"
          },
          {
            "product_id": "6",
            "id": "52",
            "datasheet_id": "10",
            "lang": "pt",
            "name": "Altura",
            "filter": "0",
            "ordemGeradores": "10"
          },
          {
            "product_id": "6",
            "id": "109",
            "datasheet_id": "20",
            "lang": "pt",
            "name": "Tração",
            "filter": "0",
            "ordemGeradores": "0"
          }
        ],
        "images": []
      },
      {
        "id": "87",
        "product_id": "7",
        "lang": "pt",
        "name": "600AJ",
        "body": "Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance600AJ.jpg\"><br>",
        "categories_id": "6",
        "media_id": "288",
        "slug": "600aj",
        "image": "assets/img/speakers/bear.jpg",
        "search": " 600AJ  Lanças Articuladas  Plataformas Aéreas  Locação  Venda novo  Trata-se de plataformas aéreas de lança articulada que permitem ultrapassar os obstáculos com muita facilidade a grandes alturas. Devido o seu grande tamanho, são movidos por motores a diesel e são usados em grandes construções. Eles podem ser usados em áreas de difícil acesso.<br><br><i><u>Obs: Altura de Trabalho = Altura da Plataforma + 1,80m do Operador.</u></i><br><img alt=\"\" src=\"http://admin.solarisbrasil.com.br/uploads/documents/Alcance600AJ.jpg\"><br>  Externo - Diesel  Plano  Desnivelado  Manutenção  Instalação  Montagem  Construção  Indústria  JLG  20,26  12,10  230  10.455  Diesel  8,82  2,49  2,57  4x4 ",
        "meta_description": "A plataforma 600AJ conta com lança articulada, atingindo um altura de 20,26 metros e motor a diesel.",
        "meta_title": "Plataforma Aérea Articulada JLG 600AJ - Solaris Brasil",
        "gerador_kva": null,
        "category": "Lanças Articuladas",
        "disponibility": [
          {
            "product_id": "7",
            "id": "10",
            "disponibility_id": "3",
            "lang": "pt",
            "name": "Locação",
            "url": "locacao-de-equipamentos"
          },
          {
            "product_id": "7",
            "id": "34",
            "disponibility_id": "4",
            "lang": "pt",
            "name": "Venda Novo",
            "url": "venda-de-equipamentos-novos"
          }
        ],
        "tags": [],
        "datasheet": [
          {
            "product_id": "7",
            "id": "82",
            "datasheet_id": "1",
            "lang": "pt",
            "name": "Marca",
            "filter": "1",
            "ordemGeradores": "1"
          },
          {
            "product_id": "7",
            "id": "61",
            "datasheet_id": "2",
            "lang": "pt",
            "name": "Altura de Trabalho (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "7",
            "id": "49",
            "datasheet_id": "3",
            "lang": "pt",
            "name": "Alcance Horizontal (m)",
            "filter": "1",
            "ordemGeradores": "0"
          },
          {
            "product_id": "7",
            "id": "64",
            "datasheet_id": "4",
            "lang": "pt",
            "name": "Capacidade (kg)",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "7",
            "id": "91",
            "datasheet_id": "6",
            "lang": "pt",
            "name": "Peso Bruto (kg)",
            "filter": "0",
            "ordemGeradores": "11"
          },
          {
            "product_id": "7",
            "id": "88",
            "datasheet_id": "7",
            "lang": "pt",
            "name": "Motor",
            "filter": "0",
            "ordemGeradores": "0"
          },
          {
            "product_id": "7",
            "id": "73",
            "datasheet_id": "8",
            "lang": "pt",
            "name": "Comprimento",
            "filter": "0",
            "ordemGeradores": "8"
          },
          {
            "product_id": "7",
            "id": "76",
            "datasheet_id": "9",
            "lang": "pt",
            "name": "Largura",
            "filter": "0",
            "ordemGeradores": "9"
          },
          {
            "product_id": "7",
            "id": "52",
            "datasheet_id": "10",
            "lang": "pt",
            "name": "Altura",
            "filter": "0",
            "ordemGeradores": "10"
          },
          {
            "product_id": "7",
            "id": "109",
            "datasheet_id": "20",
            "lang": "pt",
            "name": "Tração",
            "filter": "0",
            "ordemGeradores": "0"
          }
        ],
        "images": []
      }

    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: Item) {
    this.items.push(item);
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
