import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { MainPage } from '../pages/pages';
import { Settings } from '../providers/providers';

import { Network } from '@ionic-native/network';

@Component({
  template: `<ion-menu [content]="content">
    <ion-header>
      <ion-toolbar>
        <ion-title>
          <img src="assets/img/logo-solaris.png" alt="SOLARIS" class="logo">
        </ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list class="solaris-menu">
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          <div class="item-menu {{p.class}}"> 
            <img src={{p.icon}} class="icon-menu" alt="-">        
            <span class="menu-item-title">{{p.title}}</span>
          </div>
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  public rootPage = MainPage;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    // remover paginas de exemplo
    // { title: 'Tutorial', component: 'TutorialPage' },
    // { title: 'Welcome', component: 'WelcomePage' },
    // { title: 'Tabs', component: 'TabsPage' },
    // { title: 'Cards', component: 'CardsPage' },
    // { title: 'Content', component: 'ContentPage' },
    // { title: 'Login', component: 'LoginPage' },
    // { title: 'Signup', component: 'SignupPage' },
    // { title: 'Master Detail', component: 'ListMasterPage' },
    // { title: 'Menu', component: 'MenuPage' },
    // { title: 'Settings', component: 'SettingsPage' },
    // { title: 'Search', component: 'SearchPage' },

    // paginas do menu
    { icon:'assets/img/icons/default/home.png', class: ' active', title: 'HOME', component: 'HomePage' },
    { icon:'assets/img/icons/default/catalogo.png', class: '', title: 'CATÁLOGO', component: 'CatalogPage' }, // deativado em prod
    { icon:'assets/img/icons/default/filiais.png', class: '', title: 'FILIAIS', component: 'AffiliatedPage' },
    //{ icon:'assets/img/icons/default/simulador.png',class: '', title: 'SIMULADOR', component: 'SimulatorPage' }, // sesativado em prod
    { icon:'assets/img/icons/default/ajuda.png',class: '', title: 'AJUDA', component: 'HelpPage' },
    { icon:'assets/img/icons/default/chamada.png', class: '', title: 'CHAMADO TÉCNICO', component: 'CallPage' },
    { icon:'assets/img/icons/default/contato.png', class: '', title: 'CONTATO', component: 'ContactPage' },
    // carrinho adicionado temporáriamente
    //{ icon:'assets/img/icons/default/chamada.png', class: '', title: 'CARRINHO...', component: 'CartPage' }

    // fim paginas do menu
    
  ]

  constructor(
    private translate: TranslateService,
    platform: Platform, 
    settings: Settings, 
    private config: Config, 
    private statusBar: StatusBar, 
    private splashScreen: SplashScreen,
    private network: Network

    ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      
    });
    this.initTranslate();
    this.checkConnection();

  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'pt-br') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);

    this.removeActive()

    if (page.class === ""){
      page.class= "active";

      if (page.class === "active") {

        if(page.title === 'HOME') {
          page.icon = "assets/img/icons/active/home.png";
        }
  
        if(page.title === 'CATÁLOGO') {
          page.icon = "assets/img/icons/active/catalogo.png";
        }
  
        if(page.title === 'FILIAIS') {
          page.icon = "assets/img/icons/active/filiais.png";
        }

        if(page.title === 'SIMULADOR') {
          page.icon = "assets/img/icons/active/simulador.png";
        }
  
        if(page.title === 'AJUDA') {
          page.icon = "assets/img/icons/active/ajuda.png";
        }
  
        if(page.title === 'CHAMADO TÉCNICO') {
          page.icon = "assets/img/icons/active/chamada.png";
        }
  
        if(page.title === 'CONTATO') {
          page.icon = "assets/img/icons/active/contato.png";
        }

      }

    } else {
      page.class = "";
    }  
  }
  // workaround , rever melhor forma
  ngOnInit() {
    this.splashScreen.show();
    //this.nav.push('HomePage');
    this.setActiveDefault();    
  }

  setActiveDefault(){
    this.pages.forEach((page) => {
      if(page.component === 'HomePage'){
        page.icon = "assets/img/icons/active/home.png";
      }
    });
    
  }

  removeActive(){
    this.pages.forEach((page) => {
      if(page.class="active") {
        page.class = ""
        
        if(page.title === 'HOME') {
          page.icon = "assets/img/icons/default/home.png";
        }
  
        if(page.title === 'CATÁLOGO') {
          page.icon = "assets/img/icons/default/catalogo.png";
        }
  
        if(page.title === 'FILIAIS') {
          page.icon = "assets/img/icons/default/filiais.png";
        }

        if(page.title === 'SIMULADOR') {
          page.icon = "assets/img/icons/default/simulador.png";
        }
  
        if(page.title === 'AJUDA') {
          page.icon = "assets/img/icons/default/ajuda.png";
        }
  
        if(page.title === 'CHAMADO TÉCNICO') {
          page.icon = "assets/img/icons/default/chamada.png";
        }
  
        if(page.title === 'CONTATO') {
          page.icon = "assets/img/icons/default/contato.png";
        }

      }
    });

  }

  checkConnection(){
    // watch network for a disconnect
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.nav.setRoot('ErrorPage'); 
      console.log('sem conexão com a internet: ', this.network, 'disconnectSubscription: ', disconnectSubscription);
    });
    
  }

}
