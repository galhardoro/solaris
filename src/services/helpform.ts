import { Injectable } from '@angular/core';

@Injectable()
export class HelpFormService {
    public name: string = '';
    public email: string = '';
    public phone: string = '';
    public message: string = '';
    constructor() { }
}